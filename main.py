import re
import os
from glob import glob

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    files_unv = glob('prova_seleccio/etiquetes_sense_verificar/*')
    files_ver = glob('prova_seleccio/etiquetes_verificades/*')

    num_modified_files = 0
    num_added_boxes = 0
    num_deleted_boxes = 0

    # Comparar cada par de archivos no verificados y verificados
    for i, (im_unv, im_ver) in enumerate(zip(files_unv, files_ver)):
        path_file_unv, file_name_unv = os.path.split(im_unv)
        path_file_ver, file_name_ver = os.path.split(im_ver)
        assert file_name_unv == file_name_ver

        with open(im_unv) as file_unv, open(im_ver) as file_ver:
            lines_unv = [line.rstrip() for line in file_unv]
            lines_ver = [line.rstrip() for line in file_ver]

    ###################### Insert code here #########################


    ###################### End code here #########################

    print("added boxes", num_added_boxes)
    print("deleted boxes", num_deleted_boxes)
    print("num_modified_files0", num_modified_files)

